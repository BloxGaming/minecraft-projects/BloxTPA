package com.bloxgaming.spigot.bloxtpa

import org.bukkit.ChatColor

/**
 * Constants
 * @author Gregory Maddra
 * 2017-06-16
 */
object Constants {

    val NO_PERMISSION_ERROR = "${ChatColor.RED}You do not have permission to do that!"
    val ONLY_PLAYERS_ERROR = "Only players may use this command"
    val TARGET_SELF_ERROR = "${ChatColor.RED}You cannot target yourself unless an admin has enabled debug mode."
    val NO_SUCH_PLAYER_ERROR = "${ChatColor.RED}That player was not found!"

    val SEND_TPA_PERMISSION = "tpa.send"
    val ACCEPT_TPA_PERMISSION = "tpa.accept"
}