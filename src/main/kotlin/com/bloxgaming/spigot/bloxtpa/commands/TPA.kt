package com.bloxgaming.spigot.bloxtpa.commands

import com.bloxgaming.spigot.bloxtpa.BloxTPA
import com.bloxgaming.spigot.bloxtpa.Constants
import com.bloxgaming.spigot.bloxtpa.PendingTPACache
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.entity.Player
import org.bukkit.scheduler.BukkitRunnable

/**
 * TPA
 * @author Gregory Maddra
 * 2017-06-16
 */
class TPA : CommandExecutor {

    override fun onCommand(sender: CommandSender?, command: Command?, label: String?, args: Array<out String>): Boolean {
        if (sender is ConsoleCommandSender) {
            sender.sendMessage("Only players may execute this command!")
        } else if (sender is Player) {
            if (args.size != 1) {
                return false
            }
            val targetPlayerName = args[0]
            val targetPlayer: Player? = Bukkit.getPlayer(targetPlayerName)
            if (targetPlayer == null) {
                sender.sendMessage(Constants.NO_SUCH_PLAYER_ERROR)
                return true
            }
            if (sender == targetPlayer && !BloxTPA.debugMode) {
                sender.sendMessage(Constants.TARGET_SELF_ERROR)
                return true
            }
            if (PendingTPACache.getIfPresent(targetPlayer) != null) {
                sender.sendMessage("${targetPlayer.playerListName}${ChatColor.RED} has an outstanding TPA request")
                return true
            }
            if (sender.hasPermission(Constants.SEND_TPA_PERMISSION) && targetPlayer.hasPermission(Constants.ACCEPT_TPA_PERMISSION)) {
                sender.sendMessage("${ChatColor.GREEN}TPA Request Sent")
                targetPlayer.sendMessage("You have received a TPA Request from ${sender.playerListName}${ChatColor.RESET}." +
                        " ${ChatColor.GREEN}/tpaccept ${ChatColor.RESET} or ${ChatColor.RED}/tpdeny")
                PendingTPACache.put(targetPlayer, sender) //targetPlayer first so we can find who sent them the request
                //Let them know if TPA request expires
                object : BukkitRunnable() {
                    override fun run() {
                        if (PendingTPACache.getIfPresent(targetPlayer) == sender) {
                            sender.sendMessage("${ChatColor.RED}TPA Request to ${ChatColor.RESET}${targetPlayer.playerListName} " +
                                    "${ChatColor.RED}expired!")
                            targetPlayer.sendMessage("${ChatColor.RED}TPA Request from ${ChatColor.RESET}${sender.playerListName} " +
                                    "${ChatColor.RED}expired!")
                        }
                    }
                }.runTaskLater(BloxTPA.plugin, 20L * BloxTPA.requestValidTime)
            } else if (!targetPlayer.hasPermission(Constants.ACCEPT_TPA_PERMISSION)) {
                sender.sendMessage("${targetPlayer.playerListName} cannot accept TPA requests.")
            } else {
                sender.sendMessage(Constants.NO_PERMISSION_ERROR)
            }
        }
        return true
    }
}