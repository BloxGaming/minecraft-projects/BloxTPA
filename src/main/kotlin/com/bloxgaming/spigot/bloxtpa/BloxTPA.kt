package com.bloxgaming.spigot.bloxtpa

import com.bloxgaming.spigot.bloxtpa.commands.TPA
import com.bloxgaming.spigot.bloxtpa.commands.TPAccept
import com.bloxgaming.spigot.bloxtpa.commands.TPDeny
import org.bukkit.plugin.java.JavaPlugin

class BloxTPA : JavaPlugin() {

    companion object {
        var teleportDelay: Int = 3
        var requestValidTime: Long = 30
        var debugMode = false
        lateinit var plugin: BloxTPA
    }

    override fun onEnable() {
        logger.info("Loading BloxTPA...")

        plugin = this

        this.getCommand("tpa").executor = TPA()
        this.getCommand("tpaccept").executor = TPAccept()
        this.getCommand("tpdeny").executor = TPDeny()

        val config = this.config
        config.addDefault("TeleportDelay", teleportDelay)
        config.addDefault("RequestValidFor", requestValidTime)
        config.addDefault("DebugMode", debugMode)
        config.options().copyDefaults(true)
        this.saveDefaultConfig()

        teleportDelay = config.getInt("TeleportDelay", teleportDelay)
        requestValidTime = config.getLong("RequestValidFor", requestValidTime)
        debugMode = config.getBoolean("DebugMode", debugMode)

        logger.info("Loaded!")
    }

    override fun onDisable() {
        logger.info("Disabled!")
    }
}
