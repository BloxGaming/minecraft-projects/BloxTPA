package com.bloxgaming.spigot.bloxtpa

import com.bloxgaming.spigot.bloxtpa.commands.TPA
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import junit.framework.TestCase
import org.bukkit.Bukkit
import org.bukkit.Server
import org.bukkit.entity.Player
import org.bukkit.plugin.Plugin
import org.bukkit.scheduler.BukkitScheduler
import org.bukkit.scheduler.BukkitTask
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.powermock.api.mockito.PowerMockito
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner
import org.powermock.reflect.Whitebox

/**
 * TPATest
 * @author Gregory Maddra
 * 2017-06-18
 */
@RunWith(PowerMockRunner::class)
@PrepareForTest(Bukkit::class, BloxTPA::class)
class TPATest : TestCase() {

    val player1: Player = mock()
    val player2: Player = mock()
    val plugin: BloxTPA = mock()
    val server: Server = mock()
    val scheduler: BukkitScheduler = mock()

    val tpaCommand = TPA()

    @Before
    fun setup() {
        whenever(player1.playerListName).thenReturn("test1")
        whenever(player1.hasPermission(any<String>())).thenReturn(true)

        whenever(player2.playerListName).thenReturn("test2")
        whenever(player2.hasPermission(any<String>())).thenReturn(true)

        whenever(scheduler.runTaskLater(any<Plugin>(), any<Runnable>(), any<Long>())).thenReturn(FakeTask())

        PowerMockito.spy(Bukkit::class.java)
        PowerMockito.doReturn(player1).`when`(Bukkit::class.java, "getPlayer", "test1")
        PowerMockito.doReturn(player2).`when`(Bukkit::class.java, "getPlayer", "test2")
        PowerMockito.doReturn(null).`when`(Bukkit::class.java, "getPlayer", "Not_A_Real_Player")
        PowerMockito.doReturn(scheduler).`when`(Bukkit::class.java, "getScheduler")

        PowerMockito.spy(BloxTPA::class.java)
        Whitebox.setInternalState(BloxTPA::class.java, "plugin", plugin)

        whenever(player1.server).thenReturn(server)
    }

    @After
    fun clearCache() {
        PendingTPACache.invalidateAll()
    }

    @Test
    fun testTPA() {
        assertTrue(tpaCommand.onCommand(player1, null, null, arrayOf(player2.playerListName)))
        assertNotNull(PendingTPACache.getIfPresent(player2))
    }

    @Test
    fun testNonexistentTarget() {
        try {
            assertTrue(tpaCommand.onCommand(player1, null, null, arrayOf("Not_A_Real_Player")))
        } catch (ex: Exception) {
            ex.printStackTrace()
            fail()
        }
    }

    @Test
    fun testExistingRequests() {
        val ePlayer: Player = mock()
        PendingTPACache.put(player1, ePlayer)
        assertTrue(tpaCommand.onCommand(player2, null, null, arrayOf(player1.playerListName)))
        assertEquals(PendingTPACache.getIfPresent(player1), ePlayer)
    }

    @Test
    fun testTargetSelf() {
        assertTrue(tpaCommand.onCommand(player1, null, null, arrayOf(player1.playerListName)))
        assertNull(PendingTPACache.getIfPresent(player1))
    }

    @Test
    fun testInvalidArgs() {
        assertFalse(tpaCommand.onCommand(player1, null, null, arrayOf("arg1", "arg2")))
        assertFalse(tpaCommand.onCommand(player1, null, null, arrayOf<String>()))
    }

    private class FakeTask : BukkitTask {
        override fun getOwner(): Plugin {
            return BloxTPA.plugin
        }

        override fun getTaskId(): Int {
            return 1234
        }

        override fun cancel() {

        }

        override fun isSync(): Boolean {
            return true
        }

    }
}
